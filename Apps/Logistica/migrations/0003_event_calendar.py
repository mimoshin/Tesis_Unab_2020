# Generated by Django 3.0.4 on 2020-10-30 21:18

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('Logistica', '0002_event_calendar'),
    ]

    operations = [
        migrations.CreateModel(
            name='event_calendar',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.CharField(default='None', max_length=100)),
                ('event', models.CharField(default='None', max_length=100)),
            ],
        ),
    ]
