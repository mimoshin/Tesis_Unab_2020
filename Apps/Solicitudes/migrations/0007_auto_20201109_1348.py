# Generated by Django 3.0.4 on 2020-11-09 16:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Login', '0002_auto_20201102_1328'),
        ('Solicitudes', '0006_auto_20201102_1325'),
    ]

    operations = [
        migrations.AddField(
            model_name='info_request',
            name='event_title',
            field=models.CharField(default='event_title', max_length=40),
        ),
        migrations.AddField(
            model_name='info_request',
            name='observation',
            field=models.CharField(default='observation', max_length=300),
        ),
        migrations.AddField(
            model_name='info_request',
            name='petitioner',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='Login.Client'),
        ),
        migrations.AddField(
            model_name='info_request',
            name='specification',
            field=models.CharField(default='especification', max_length=100),
        ),
        migrations.AddField(
            model_name='info_request',
            name='status',
            field=models.CharField(default='status', max_length=20),
        ),
        migrations.AddField(
            model_name='info_request',
            name='time_create',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
